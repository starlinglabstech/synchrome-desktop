import Vue from 'vue';
import Vuetify from 'vuetify';
import VueElectron from 'vue-electron';
import Vuelidate from 'vuelidate';
import moment from 'moment';

import App from './App';
import router from './router';
import store from './store';

import http from './services/http';

Vue.use(Vuetify);
Vue.use(VueElectron);
Vue.use(Vuelidate);
Vue.config.productionTip = false;

http.init();
moment.locale('id');

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>',
}).$mount('#app');
