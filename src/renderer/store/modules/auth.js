import http from '../../services/http';
import auth from '../../services/auth';

const emptyUser = () => ({
  id: '',
  name: '',
  email: '',
  role: {
    id: '',
    name: '',
  },
});

export default {
  namespaced: true,

  state: {
    user: {
      id: '',
      name: '',
      email: '',
      role: {
        id: '',
        name: '',
      },
    },
    isLogged: false,
  },

  getters: {
    userId: state => state.user.id,
    userName: state => state.user.name,
    userEmail: state => state.user.email,
    userRole: state => state.user.role,
    isLogged: state => state.isLogged,
  },

  mutations: {
    setUser(state, user) {
      state.user = user;
      state.isLogged = true;
    },

    clearUser(state) {
      state.user = emptyUser;
      auth.clearActiveUser();
    },
  },

  actions: {
    updateProfile(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 200) {
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.patch(`users/${context.getters.userId}`, payload, successCallback, errorCallback);
      });
    },

    logout(context) {
      context.commit('clearUser');
    },
  },
};
