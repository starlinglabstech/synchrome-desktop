import request from 'superagent';
import auth from '../../services/auth';
import http from '../../services/http';

export default {
  namespaced: true,

  state: {
    data: [],
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.get('additional-events', successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 201) {
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.post('additional-events', payload, successCallback, errorCallback);
      });
    },

    fetch(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.get(`additional-events/${id}`, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const callback = (err, res) => {
          if (err) {
            reject(err);
          }

          if (res.status === 200) {
            resolve();
          }
        };

        const {
          id,
          title,
          start,
          end,
          eventCategoryId,
          employees,
          absenceTypeId,
          permit,
        } = payload;

        const req = request.post(`${http.baseURL}/additional-events/${id}`)
          .set('Authorization', `Bearer ${auth.token}`)
          .field('_method', 'PATCH')
          .field('title', title)
          .field('start', start)
          .field('event_category_id', eventCategoryId)
          .field('employees[]', employees)
          .field('absence_type_id', absenceTypeId);

        if (typeof end !== 'undefined') {
          req.field('end', end);
        }

        if (typeof permit !== 'undefined') {
          req.attach('permit', permit);
        }

        req.end(callback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 200) {
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.delete(`additional-events/${id}`, successCallback, errorCallback);
      });
    },
  },
};
