import http from '../../services/http';

export default {
  namespaced: true,

  state: {
    data: [],
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.get('users', successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 201) {
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.post('users', payload, successCallback, errorCallback);
      });
    },

    fetch(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.get(`users/${id}`, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        let data;

        if (payload.password !== null) {
          data = {
            email: payload.email,
            name: payload.name,
            role_id: payload.role_id,
            password: payload.password,
            password_conf: payload.password_conf,
          };
        } else {
          data = {
            email: payload.email,
            name: payload.name,
            role_id: payload.role_id,
          };
        }

        const successCallback = (res) => {
          if (res.status === 200) {
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.patch(`users/${payload.id}`, data, successCallback, errorCallback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 200) {
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.delete(`users/${id}`, successCallback, errorCallback);
      });
    },
  },
};
