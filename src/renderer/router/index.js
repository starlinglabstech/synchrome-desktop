import Vue from 'vue';
import Router from 'vue-router';

import LoginForm from '../components/LoginForm';
import DashboardIndex from '../components/Dashboard/DashboardIndex';
import ProfileEdit from '../components/Profile/ProfileEdit';
import UserIndex from '../components/User/UserIndex';
import UserCreate from '../components/User/UserCreate';
import UserEdit from '../components/User/UserEdit';
import AdditionalEventIndex from '../components/AdditionalEvent/AdditionalEventIndex';
import AdditionalEventCreate from '../components/AdditionalEvent/AdditionalEventCreate';
import AdditionalEventEdit from '../components/AdditionalEvent/AdditionalEventEdit';
import EmployeeIndex from '../components/Employee/EmployeeIndex';
import EmployeeDetail from '../components/Employee/EmployeeDetail';
import CalendarIndex from '../components/Calendar/CalendarIndex';
import ReportIndex from '../components/Report/ReportIndex';

Vue.use(Router);

export default new Router({
  routes: [
    { path: '/', name: 'login', component: LoginForm },
    { path: 'dashboard', name: 'dashboard', component: DashboardIndex },
    { path: 'profile', name: 'profile', component: ProfileEdit },
    { path: 'users', name: 'users.index', component: UserIndex },
    { path: 'users/new', name: 'users.create', component: UserCreate },
    { path: 'users/:id', name: 'users.edit', component: UserEdit },
    { path: 'additional-events', name: 'additional_events.index', component: AdditionalEventIndex },
    { path: 'additional-events/new', name: 'additional_events.create', component: AdditionalEventCreate },
    { path: 'additional-events/:id', name: 'additional_events.edit', component: AdditionalEventEdit },
    { path: 'employees', name: 'employees.index', component: EmployeeIndex },
    { path: 'employees/:id', name: 'employees.detail', component: EmployeeDetail },
    { path: 'calendars', name: 'calendars.index', component: CalendarIndex },
    { path: 'reports', name: 'reports.index', component: ReportIndex },
    { path: '*', redirect: '/' },
  ],
});
