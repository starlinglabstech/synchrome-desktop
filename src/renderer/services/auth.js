import jwt from 'jsonwebtoken';
import store from '../store';

const emptyUser = () => ({
  id: '',
  name: '',
  email: '',
  role: {
    id: '',
    name: '',
  },
});

export default {
  token: '',
  user: {
    id: '',
    name: '',
    email: '',
    role: {
      id: '',
      name: '',
    },
  },

  init(token) {
    const decodedToken = jwt.decode(token, { complete: true });
    this.token = token;
    this.user = decodedToken.payload.user;
    this.setActiveUser(this.user);
  },

  setActiveUser(user) {
    store.commit('auth/setUser', user);
  },

  clearActiveUser() {
    this.token = '';
    this.user = emptyUser;
  },
};
