import {
  required,
  requiredIf,
  sameAs,
} from 'vuelidate/lib/validators';

export default {
  email: {
    required,
  },
  name: {
    required,
  },
  password: {
    required: requiredIf(model => model.edit === false),
  },
  password_conf: {
    required: requiredIf(model => model.password !== ''),
    sameAsPassword: sameAs('password'),
  },
};
