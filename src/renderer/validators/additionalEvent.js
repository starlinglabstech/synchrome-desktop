import { required } from 'vuelidate/lib/validators';

export default {
  title: {
    required,
  },
  start: {
    required,
  },
  event_category_id: {
    required,
  },
  employees: {
    required,
  },
};
