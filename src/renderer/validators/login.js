import { required } from 'vuelidate/lib/validators';

export default {
  email: {
    required,
  },
  password: {
    required,
  },
};
