import {
  required,
  requiredIf,
  sameAs,
} from 'vuelidate/lib/validators';

export default {
  email: {
    required,
  },
  name: {
    required,
  },
  password: {},
  password_conf: {
    required: requiredIf(model => model.password !== ''),
    sameAsPassword: sameAs('password'),
  },
};
