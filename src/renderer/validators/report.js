import {
  required,
  requiredIf,
} from 'vuelidate/lib/validators';

export default {
  selectedReportCategory: {
    required,
  },
  selectedEmployee: {
    required: requiredIf(model => model.selectedReportCategory.id === 'scan_log'),
  },
  start: {
    required,
  },
  end: {
    required,
  },
};
